/*
   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include <stdio.h>

#include "esp_system.h"
#include "driver/gpio.h"


#include <ESP8266WiFi.h>

/******************************************************************************
 * FunctionName : app_main
 * Description  : entry of user application, init user function here
 * Parameters   : none
 * Returns      : none
*******************************************************************************/
void app_main(void)
{
    printf("Welcome to the jungle");

    gpio_config_t pin3;
    /* pin for red led  */
    pin3.mode = GPIO_MODE_OUTPUT;
    pin3.pin_bit_mask = GPIO_Pin_12;
    pin3.pull_down_en = 0;
    pin3.pull_up_en = 0;
    gpio_config(&pin3);

    gpio_set_level(GPIO_NUM_12, 1) ;

    gpio_config_t pin5;
    /* pin for red led  */
    pin5.mode = GPIO_MODE_OUTPUT;
    pin5.pin_bit_mask = GPIO_Pin_14;
    pin5.pull_down_en = 0;
    pin5.pull_up_en = 0;
    gpio_config(&pin5);

    gpio_set_level(GPIO_NUM_14, 1) ;

    gpio_config_t pin8;
    /* pin for red led  */
    pin8.mode = GPIO_MODE_OUTPUT;
    pin8.pin_bit_mask = GPIO_Pin_15;
    pin8.pull_down_en = 0;
    pin8.pull_up_en = 0;
    gpio_config(&pin8);

    gpio_set_level(GPIO_NUM_15, 1) ;
}

